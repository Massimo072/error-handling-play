package com.example.handelingerrors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandlingErrorsApplication {

  public static void main(String[] args) {
    SpringApplication.run(HandlingErrorsApplication.class, args);
  }

}
