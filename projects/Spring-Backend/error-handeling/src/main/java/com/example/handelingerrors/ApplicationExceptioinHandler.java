package com.example.handelingerrors;

import io.undertow.server.RequestTooBigException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;

import java.io.IOException;

@ControllerAdvice
public class ApplicationExceptioinHandler {

  @ExceptionHandler(RequestTooBigException.class)
  public ResponseEntity<UserResponse> handleRequestTooBigException(RequestTooBigException exception){
    return  new ResponseEntity<>(new UserResponse("File size is over 5MB", exception.getMessage().toString()), HttpStatus.BAD_REQUEST);
  }

  final class UserResponse {
    private String message;
    private String details;

    UserResponse(String message, String details) {
      this.message = message;
      this.details = details;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    public String getDetails() {
      return details;
    }

    public void setDetails(String details) {
      this.details = details;
    }
  }
}
