package com.example.handelingerrors.controller;

import com.example.handelingerrors.NotFoundException;
import io.undertow.server.RequestTooBigException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartException;

import java.io.IOException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ErrorController {

  @PostMapping(value = "/errors/add")
  public ResponseEntity postMovie(   ) throws RequestTooBigException {
   // System.out.println(error.details);
    if (0 ==0 ) {
        System.out.println("in if ");
      throw new RequestTooBigException("shte");
    }
    System.out.println("after if");
    return new ResponseEntity<>("tsrtsrts", HttpStatus.BAD_REQUEST);
// return new ResponseEntity<>(error, HttpStatus.OK);
  }

  public static class UserResponse {
    private String message;
    private String details;

    UserResponse(String message, String details) {
      this.message = message;
      this.details = details;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    public String getDetails() {
      return details;
    }

    public void setDetails(String details) {
      this.details = details;
    }
  }

}
