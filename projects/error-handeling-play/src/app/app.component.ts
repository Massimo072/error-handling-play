import {Component, OnInit} from '@angular/core';
import {ErrorService} from './error.service';
import {HttpErrorResponse} from "@angular/common/http";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'error-handeling-play';

  error: string;
  submitted = false;
  // ur:  UserResponse = new UserResponse();


  constructor(private errorService: ErrorService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    // this.ur.message = "hello";
    // this.ur.details = this.error;
    this.errorService.sendError(this.error).subscribe(result => {
      alert(result)
      },(e : HttpErrorResponse) => {
      alert(e.error.message + " " + e.error.details)
      }
    );
  }


}
