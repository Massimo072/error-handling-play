import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  private baseUrl = 'http://localhost:8080/api/errors';

  constructor(private http: HttpClient) { }

  sendError(error: String): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/add`, error);

  }
}
